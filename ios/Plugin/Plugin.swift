import Foundation
import Capacitor
import AVFoundation

/**
 * Please read the Capacitor iOS Plugin Development Guide
 * here: https://capacitor.ionicframework.com/docs/plugins/ios
 */
@objc(CapacitorQRScanner)
public class CapacitorQRScanner: CAPPlugin, QRScannerViewControllerDelegate {
    
    
    
    
    var call: CAPPluginCall?
    var qrScannerViewController: QRScannerViewController?
    
    @objc func scan(_ call: CAPPluginCall) {
        self.call = call
        
        if self.bridge.isSimulator() || !UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
          self.bridge.modulePrint(self, "Camera not available in simulator")
          self.bridge.alert("Camera Error", "Camera not available in Simulator")
          call.error("Camera not available while running in Simulator")
          return
        }
        
        DispatchQueue.main.async {
            self.qrScannerViewController = QRScannerViewController()
            self.qrScannerViewController!.delegate = self;
         }
        

        AVCaptureDevice.requestAccess(for: .video) { granted in
            if granted {
              DispatchQueue.main.async {

                self.bridge.viewController.present(self.qrScannerViewController!, animated: true, completion: nil)
              }
            } else {
                call.error("User denied access to camera")
            }
        }
        
    }
    
    func didFoundCode(code: String) {
        self.call?.success(["code": code]);
    }

}
