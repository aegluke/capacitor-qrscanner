## General
Current version works with iOS and Android
There are no styling options or camera settings now.

## Installation

```
npm i @johnbraum/capacitor-qrscanner
```  
  
In your specific view:  
```
import { Plugins } from '@capacitor/core';
const { CapacitorQRScanner } = Plugins;
```  
And then just call the method:  
```
let result = await CapacitorQRScanner.scan();
```

Access the found code `result.code`

## Bugs / Feedback
If you have problems or questions:  
https://bitbucket.org/golbros/capacitor-qrscanner/issues
